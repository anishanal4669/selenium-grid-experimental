import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.net.URL;


public class SeleniumGridTests {

    DesiredCapabilities capabilities = new DesiredCapabilities();
    public static WebDriver driver;

    @BeforeClass
    public void initSetup() throws MalformedURLException, InterruptedException {
        capabilities.setBrowserName("firefox");
        capabilities.setPlatform(Platform.LINUX);

        //ChromeOptions
        ChromeOptions options = new ChromeOptions();
        options.merge(capabilities);

        String hubURL = "http://selenium_grid_ui:port/wd/hub";
        driver = new RemoteWebDriver(new URL(hubURL), options);
        driver.get("https://ui.cogmento.com/");
        Thread.sleep(5000);
    }

    @Test
    public void validatePageTitle(){
        String expectedPageTitle = "Cogmento CRM";
        String actualPageTitle = driver.getTitle();
        Assert.assertEquals(actualPageTitle, expectedPageTitle);
    }

    @AfterClass
    public void tearDown(){
        driver.quit();
    }

}
